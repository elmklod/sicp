(define (Y-comb depth init)
    (let ((counter 0))
      (lambda (Y g)
        (if (= counter depth)
            init
            (begin (set! counter (+ counter 1))
              (g (Y Y g)))))))

; Of course, if one decides that let is cheating, then
(define (Y-comb depth init)
    ((lambda (counter)
      (lambda (Y g)
        (if (= counter depth)
            init
            (begin (set! counter (+ counter 1))
              (g (Y Y g))))))
     0))

; if define is also considered cheating
; then this procedure does the same thing as Y-comb
(lambda (depth init)
    (let ((counter 0))
      (lambda (Y g)
        (if (= counter depth)
            init
            (begin (set! counter (+ counter 1))
              (g (Y Y g)))))))

; e.g.
(let ((Y-current (Y-comb 10 1)))
    (Y-current Y-current (lambda (x) (* 2 x))))

((lambda (Y-current)
   (Y-current Y-current (lambda (x) (* 2 x))))
 ((lambda (depth init)
    (let ((counter 0))
      (lambda (Y g)
        (if (= counter depth)
            init
            (begin (set! counter (+ counter 1))
              (g (Y Y g)))))))
  10
  1))

(define (Y-recur f depth init)
    (let ((Y-current (Y-comb depth init)))
      (Y-current Y-current f)))

(define (Y-recur f depth init)
    (define (Y-comb)
      (let ((counter 0))
        (lambda (Y g)
          (if (= counter depth)
              init
              (begin (set! counter (+ counter 1))
                (g (Y Y g)))))))
    (let ((Y-current (Y-comb)))
      (Y-current Y-current f)))

; (Y-recur (lambda (x) (* 3 x)) 5 1)

(define (Y-recur f depth init)
    (let ((Y-current (let ((counter 0))
        (lambda (Y g)
          (if (= counter depth)
              init
              (begin (set! counter (+ counter 1))
                (g (Y Y g))))))))
      (Y-current Y-current f)))

(define (Y-recur f depth init)
    (define (Y-comb)
      (let ((counter 0))
        (lambda (Y)
          (if (= counter depth)
              init
              (begin (set! counter (+ counter 1))
                (f (Y Y)))))))
    (let ((Y-current (Y-comb)))
      (Y-current Y-current)))

(define (Y-recur f depth init)
    (let ((Y-current
            (let ((counter 0))
              (lambda (Y)
                (if (= counter depth)
                    init
                  (begin (set! counter (+ counter 1))
                         (f (Y Y))))))))
      (Y-current Y-current)))

;;; This is the Y combinator, say hi
(lambda (f depth init)
    (let ((Y-current
            (let ((counter 0))
              (lambda (Y)
                (if (= counter depth)
                    init
                  (begin (set! counter (+ counter 1))
                         (f (Y Y))))))))
      (Y-current Y-current)))

; In all its glory
(lambda (f)
    (let ((Y-current
             (lambda (Y)
               (f (Y Y)))))
      (Y-current Y-current)))