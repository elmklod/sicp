(define (make-register name)
  (let ((tracing false)           ;;; initially false so that pc and flag are not traced from the start since it clutters the tracing output greatly
        (contents '*unassigned*))
    (define (dispatch message)
      (cond ((eq? message 'get) contents)
            ((eq? message 'set)
             (lambda (value)
               (if tracing
                   (begin (newline)
                          (display "Register: ")
                          (display name)
                          (newline)
                          (display "Old contents: ")
                          (display contents)
                          ; now there is a leakage,
                          ; a value is accessed without
                          ; using the designated accessor
                          ; (dispatch 'get), and indeed,
                          ; it is safer but slower. Take heed.
                          (newline)
                          (display "New contents: ")
                          (display value)
                          (newline)))
               (set! contents value)))
            ((eq? message 'trace-on)
             (set! tracing true)
             'done)
            ((eq? message 'trace-off)
             (set! tracing false)
             'done)
            (else
             (error "Unknown request:
                     REGISTER"
                    message))))
    dispatch))

(define (make-machine register-names 
                      ops 
                      controller-text)
  (let ((machine (make-new-machine)))
    (for-each (lambda (register-name)
                ((machine 'allocate-register) 
                 register-name))
              register-names)
    ((machine 'install-operations) ops)
    (assemble controller-text machine
     (lambda (insts labels)
      ((machine 'install-instruction-sequence) insts)
      ((machine 'install-label-entries) labels)))
    machine))

(define (assemble controller-text machine recieve)
  (extract-labels controller-text
    (lambda (insts labels)
      (update-insts! insts labels machine)
      (recieve insts labels))))                     
; since update-insts! mutates, this application of recieve need not come last,
; but only because it can be proved that it is used purely for side effects
; and not its value
; but in general recieve application must come last
; and I adhere to this principle since ignoring it adds no benefit here whatsoever


(define (update-insts! insts labels machine)
  (let ((pc (get-register machine 'pc))
        (flag (get-register machine 'flag))
        (stack (machine 'stack))
        (ops (machine 'operations)))
    (for-each
     (lambda (inst)
       (set-instruction-execution-proc!
        inst
        (make-execution-procedure
         (instruction-text inst)
         labels
         machine
         pc
         flag
         stack
         ops))
       (set-instruction-labels! inst labels))
     insts)))

(define (make-instruction text)
  (cons (list text '() '()) '()))
(define (instruction-text inst) (caar inst))
(define (instruction-execution-proc inst)
  (cdr inst))
(define (set-instruction-execution-proc!
         inst
         proc)
  (set-cdr! inst proc))

(define (instruction-labels inst)
 (cadar inst))

(define (instruction-break? inst)
 (not (null? (instruction-break-info inst))))

(define (instruction-break-info inst)
 (caddar inst))

(define (set-instruction-break! inst info)
 (set-car! (cddar inst) info)
 'done)

(define (remove-instruction-break! inst)
 (set-car! (cddar inst) '())
 'done)

(define (set-instruction-labels! inst labels)                                                                                                                 
; the alternativeis to recurse on insts in the update procedure, but this one
; works as well because here unique lists have unique first elements.
; and having the same first element means being the same list.
; but this path requires additional worries about list not being nil to check its first el.
; no instruction has labels that point to the end of sequnce,
; so such labels are skipped safely.
 (set-car!
  (cdar inst)
  (map car
       (filter (lambda (l-entry)
                (let ((label-position (cdr l-entry)))
                 (and (pair? label-position)
                      (eq? inst (car label-position)))))
         labels))))

(define (make-new-machine)
  (let ((pc (make-register 'pc))
        (flag (make-register 'flag))
        (stack (make-stack))
        (executed-instructions-count 0)
        (tracing true)
	(label-entries '())
	(current-machine-continuation '())
        (the-instruction-sequence '()))
    (let ((the-ops
           (list (list 'initialize-stack
                       (lambda () (stack 'initialize)))
                 ;;**next for monitored stack (as in section 5.2.4)
                 ;;  -- comment out if not wanted
                 (list 'print-stack-statistics
                       (lambda () (stack 'print-statistics)))))
          (register-table
           (list (list 'pc pc) (list 'flag flag))))
      (define (allocate-register name)
        (if (assoc name register-table)
            (error "Multiply defined register: " name)
            (set! register-table
                  (cons (list name (make-register name))
                        register-table)))
        'register-allocated)
      (define (lookup-register name)
        (let ((val (assoc name register-table)))
          (if val
              (cadr val)
              (error "Unknown register:" name))))
      (define (execute)
        (let ((insts (get-contents pc)))
          (if (null? insts)
              'done
              (let* ((inst (car insts))
	             (machine-continuation
                      (lambda ()
		       (if tracing
                           (begin (let ((labels (instruction-labels inst)))
                                   (if (not (null? labels))
                                       (begin (newline)
                                       (display "Labels: ")
                                       (display labels))))
                                  (newline)
                                  (display "Instruction: ")
                                  (display (instruction-text inst)) 
                                  (newline)))
                       ((instruction-execution-proc inst))
                       (set! executed-instructions-count
                        (+ 1 executed-instructions-count))
                       (execute))))
	       (cond ((instruction-break? inst)
	              (set! current-machine-continuation machine-continuation)
		      (newline)
		      (display "Breakpoint reached at: ")
		      (display (instruction-break-info inst))
		      (newline))
		     (else (machine-continuation)))))))
      (define (change-registers-tracing message register-names)
       (if (null? register-names)
           (error "No register names provided")) ; what are the consequences for not branching and using a conditional error like a statement?
       (for-each
        (lambda (name)
         ((lookup-register name) message))
        register-names)
       'done)
      (define (proceed)
       (if (null? current-machine-continuation)
           (error "Can't continue from a breakpoint as no breakpoint has been reached")
	   (let ((proc current-machine-continuation))
	    (set! current-machine-continuation '())
	    (proc))))
      (define (reach-instruction-at label n)
       (define (iter pos n)
        (cond ((null? pos)
	       (error "There is no instruction at this position(End of instruction sequence reached): " (list label n)))
	      ((= n 1)
	       (car pos))
	      ((> n 1)
	       (iter (cdr pos) (- n 1)))
	      (else (error "Cannot go backwards or to the label :" (list label n)))))
       (iter (lookup-label label-entries label) n))
      (define (dispatch message)
        (cond ((eq? message 'start)
               (set-contents! pc the-instruction-sequence)
               (execute))
	      ((eq? message 'proceed)
	       (proceed))
              ((eq? message 'install-instruction-sequence)
               (lambda (seq) (set! the-instruction-sequence seq)))
              ((eq? message 'allocate-register) allocate-register)
              ((eq? message 'get-register) lookup-register)
              ((eq? message 'install-operations)
               (lambda (ops) (set! the-ops (append the-ops ops))))
              ((eq? message 'stack) stack)
              ((eq? message 'operations) the-ops)
              ((eq? message 'executed-instructions-count)
               executed-instructions-count)
              ((eq? message 'reset-instructions-count)
               (set! executed-instructions-count 0)
               'done)
              ((eq? message 'trace-off)
               (set! tracing false)
               'done)
              ((eq? message 'trace-on)
               (set! tracing true)
               'done)
              ((eq? message 'registers-tracing-off)
               (lambda (registers)
                (change-registers-tracing 'trace-off registers)))
              ((eq? message 'registers-tracing-on)
               (lambda (registers)
                (change-registers-tracing 'trace-on registers)))
              ((eq? message 'install-label-entries)
	       (lambda (entries)
	        (set! label-entries entries)))
	      ((eq? message 'set-breakpoint)
	       (lambda (label n)
	        (set-instruction-break! (reach-instruction-at label n) (list label n))))
	      ((eq? message 'cancel-breakpoint)
	       (lambda (label n)
	        (remove-instruction-break! (reach-instruction-at label n))))
	      ((eq? message 'cancel-all-breakpoints)
	       (for-each remove-instruction-break! the-instruction-sequence)
	       'done)
	      (else (error "Unknown request -- MACHINE" message))))
      dispatch)))

(define (lookup-label labels label-name)
  (let ((val (assoc label-name labels)))
    (if val
        (cdr val)
        (error "Undefined label: " 
               label-name))))

(define (proceed-machine machine)
 (machine 'proceed))

(define (set-breakpoint machine label n)
 ((machine 'set-breakpoint) label n))

(define (cancel-breakpoint machine label n)
 ((machine 'cancel-breakpoint) label n))

(define (cancel-all-breakpoints machine)
 (machine 'cancel-all-breakpoints))
