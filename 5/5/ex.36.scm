construct-arglist along with code-to-get-rest-args
is responsible for order of evaluation.
In the current state, the compiler evaluates
from the last to the first.

Yes, the order of evaluation affects the compiler and
the reselting code. The first operand does not need
argl register saved. The last operand is the same in this
regard.
operands in the middle all require argl to be saved if they
are not evaluated last

The efficiency question does arise when saving and restoring
registers.
For operands that are neither last nor first, there
are in general few ways to optimize their calls(sometimes
there are some, but those are special cases).

Going last->first preserves env for every operand but the first.
It also preserves argl for every operand but the last.

Going first->last preserves env for every operand but the last.
It also preserves argl for every operand but the first.

These two variants are equivalent in what they do.

THEIR last operand does not require env save
and THEIR first operand does not require argl save.

I won't change a thing, any other evaluation order is far
harder to make an excuse for its use. It's still possible to
choose the first that gets the arglist save removal and
the last that gets env save removal in any order, though.
