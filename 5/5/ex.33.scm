  (assign val (op make-compiled-procedure) (label entry2) (reg env))
  (goto (label after-lambda1))
entry2
  (assign env (op compiled-procedure-env) (reg proc))
  (assign env (op extend-environment) (const (n)) (reg argl) (reg env))
  (save continue)
  (save env)
  (assign proc (op lookup-variable-value) (const =) (reg env))
  (assign val (const 1))
  (assign argl (op list) (reg val))
  (assign val (op lookup-variable-value) (const n) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch17))
compiled-branch16
  (assign continue (label after-call15))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch17
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
after-call15
  (restore env)
  (restore continue)
  (test (op false?) (reg val))
  (branch (label false-branch4))
true-branch5
  (assign val (const 1))
  (goto (reg continue))
false-branch4
  (assign proc (op lookup-variable-value) (const *) (reg env))
  (save continue)
  (save proc)
  (save env)             ;;;;;;;;;;;;; here
  (assign proc (op lookup-variable-value) (const factorial-alt) (reg env))
  (save proc)
  (assign proc (op lookup-variable-value) (const -) (reg env))
  (assign val (const 1))
  (assign argl (op list) (reg val))
  (assign val (op lookup-variable-value) (const n) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch8))
compiled-branch7
  (assign continue (label after-call6))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch8
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
after-call6 
  (assign argl (op list) (reg val))
  (restore proc)
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch11))
compiled-branch10
  (assign continue (label after-call9))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch11
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
after-call9 
  (assign argl (op list) (reg val))
  (restore env) ;;;;;;;;;; here
  ;;;; No argl register save
  (assign val (op lookup-variable-value) (const n) (reg env))
  ;;;; arglist instructions for values switched places
  ;;;; No arglist register restore
  (assign argl (op cons) (reg val) (reg argl))
  (restore proc)
  (restore continue)
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch14))
compiled-branch13
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch14
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
  (goto (reg continue))
after-call12
after-if3
after-lambda1
  (perform (op define-variable!) (const factorial-alt) (reg val) (reg env))
  (assign val (const ok)))


There are 5 differences:
2 of them is the saving of environment before the instruction sequence for the last operand.
That is because
first evaluating (factorial-alt (- n 1))
and then evaluating n
requires preserving env around the first evaluation.
That resulted from the change of operand order in the if-alternative clause * application,
since the last argument is now the procedure application, it by its design is belived to modify every register,
unlike variable lookup which only modifies target reg.

2 of changes is the missing of argl save and restore around the first operand instruction
Since the first operand is the variable n, it is evaluated last.
Since by design the lookup will done with target val(since it's an operand to procedure)
The instruction sequence does not modify any other register.
So there is no need to preserve argl around its instruction sequence.

The last change is the swapping of instruction sequences for operands in * application,
It's because the order of operands was changed, while the order of operand evaluation was not(and is fixed)


Concerning efficiency, Hard to tell. The last change does not really affect anything.
In general only pointers are saved on stack here, so saving env or saving argl does not make any difference.
The versions in fact share the same maximum stack-depth formula(need to check really)

But it all depends on how environments are handled.
Usually argl contents are small unlike env contents.
argl if stored on heap does not take much space,
while env may take more
if they are stored on heap(this is especially important if stack saves actually save poinsters with actual data stored on heap while register contents are not),
env contents will usually take more space
If they are all stored on heap(or on heap in the implementation language)
Then the garbage collection timing and memory freed is an important factor
Unlike argl, which will be in use during the procedure application of * and will not be gc-ed until then,
env will not be needed by the time the application of * takes place,
More than that, env contents, after the evaluation of n variable, and (- n 1), factorial-alt var, * var won't be needed at all, since factorial-alt stores its
own environment and it may not be the same environment stored at env, so the original env may be gc-ed earlier, right after the evaluations
of 5 variable lookups(2 of which are the same lookups).
So the original factorial, considering the storage strategy used above, is more efficient, since it allows for the earlier garbage-collection
of original env, unlike, this alternative version, which only allows for garbage collection to take place after the evaluation of the
factorial-alt procedure application, and the minimal time before garbage collection of env becomes O(n), while arl always has O(n)
Env in the original procedure has minimal time before garbage collection that is O(1) - variable lookup +
a fixed overhead of proc application code of primitive procedures and proc-body + 1 element seq + if + if-pred (+ if-consequent if pred is true)
is constant time since the env
of procedure always has the same structure(pray for determinism)
and special forms evaluation design
evaluations of proc applications, proc bodies, if-s, if-predicates, if-consequents do not depend on the order of operands in * application
and add fixed overhead in both memory usage and time comlexity in each proc application
Therefore, the original may be more effective under certain circumstances

