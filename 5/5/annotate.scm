(define (annotate seq)
 (for-each (lambda (e)
            (if (not (symbol? e))
                (display "  "))
            (display e)
            (newline))
           (statements seq)))
;(annotate 
; (compile
;  '(define (factorial n)
;    (define (iter product counter)
;     (if (> counter n)
;         product
;         (iter (* counter product)
;               (+ counter 1))))
;    (iter 1 1))
;  'val
;  'next))
