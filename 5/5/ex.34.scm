(define (annotate seq)
 (for-each (lambda (e)
            (if (not (symbol? e))
                (display "  "))
            (display e)
            (newline))
           (statements seq)))

; I guess I'm really stupid for not writing this procedure sooner for ex.33
; But better late than never


(annotate 
 (compile
  '(define (factorial n)
    (define (iter product counter)
     (if (> counter n)
         product
         (iter (* counter product)
               (+ counter 1))))
    (iter 1 1))
  'val
  'next))




  (assign val (op make-compiled-procedure) (label entry2) (reg env))
  (goto (label after-lambda1))
entry2
  (assign env (op compiled-procedure-env) (reg proc))
  (assign env (op extend-environment) (const (n)) (reg argl) (reg env))
  ;;; the internal definition is evaluated
  (assign val (op make-compiled-procedure) (label entry7) (reg env))
  (goto (label after-lambda6))
entry7
  (assign env (op compiled-procedure-env) (reg proc))
  (assign env (op extend-environment) (const (product counter)) (reg argl) (reg env))
  ;;; the evaluation of if predicate in iter takes place
  (save continue) ;;; wrapped around predicate sequence, stack depth always 2
  (save env)
  (assign proc (op lookup-variable-value) (const >) (reg env))
  (assign val (op lookup-variable-value) (const n) (reg env))
  (assign argl (op list) (reg val))
  (assign val (op lookup-variable-value) (const counter) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch22))
compiled-branch21
  (assign continue (label after-call20))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch22
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
after-call20
  (restore env)
  (restore continue) ;;; stack empty after evaluating predicate
  (test (op false?) (reg val))
  (branch (label false-branch9))
true-branch10
  (assign val (op lookup-variable-value) (const product) (reg env))
  (goto (reg continue))
false-branch9
  ;;; iter application in the body of iter
  (assign proc (op lookup-variable-value) (const iter) (reg env))
  (save continue)
  (save proc)
  ;;; 2 saves above are because both operands are procedure application(all reg modified)
  (save env) ;;; This save is done because the last operand may modify env, while the first
             ;;; needs it, stack depth 3
  ;;; (+ counter 1) sequence
  (assign proc (op lookup-variable-value) (const +) (reg env))
  (assign val (const 1))
  (assign argl (op list) (reg val))
  (assign val (op lookup-variable-value) (const counter) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch16))
compiled-branch15
  (assign continue (label after-call14))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch16
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
after-call14
  (assign argl (op list) (reg val))
  (restore env) ;;; restored for (* counter product), stack depth 2
  ;;; the sequence for (* counter product)
  (save argl)   ;;; arglist is saved because (* counter product) makes its own arglist
  ;;;; stack depth 3
  (assign proc (op lookup-variable-value) (const *) (reg env))
  (assign val (op lookup-variable-value) (const product) (reg env))
  (assign argl (op list) (reg val))
  (assign val (op lookup-variable-value) (const counter) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch13))
compiled-branch12
  (assign continue (label after-call11))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))
primitive-branch13
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
after-call11
  (restore argl) ;;; arglist is restored for the final update, stack depth 2
  (assign argl (op cons) (reg val) (reg argl))
  (restore proc)
  (restore continue) ;;; stack empty right before the evaluation of iter body
  ;;; restored for the iter application inside iter      
  ;;; since it takes the linkage from if, which takes it from sequence, which takes it from
  ;;; procedure body, the linkage is 'return, the target is 'val
  ;;; since the compiler produces the tail recursive call for such(since there is no reason
  ;;; to return elsewhere and then jump to the saved continue - there are no things to do 
  ;;; before the jump to the saved continue, so instead of 2 jumps, just jump the original    
  ;;; continue
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch19))
compiled-branch18
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val)) ;;; the procedure body will do the jump to continue anyways
primitive-branch19
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
  (goto (reg continue)) ;;; the explicit jump to continue
after-call17
after-if8
after-lambda6
  ;;; internal definition takes place
  (perform (op define-variable!) (const iter) (reg val) (reg env))
  (assign val (const ok))
  ;;; iter procedure application inside the body of factorial
  (assign proc (op lookup-variable-value) (const iter) (reg env))
  (assign val (const 1))
  (assign argl (op list) (reg val))
  (assign val (const 1))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch5))
compiled-branch4
  ;;; since the last sequence element will get 'return in linkage and 'val in target,
  ;;; the procedure application utilises the val target and return linkage directly during
  ;;; the procedure body evaluation, instead of doing unneccessary jumps
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val)) ;;; tail rec, no saves done
primitive-branch5
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
  (goto (reg continue))  ;;; explicit jump to continue
after-call3
after-lambda1
  (perform (op define-variable!) (const factorial) (reg val) (reg env))
  (assign val (const ok))


;;;; * empty means that the stack holds nothing generated during the factorial call 
;;;; as one can see, the stack is always empty when before
;;;; the iter procedure application,
;;;; which means that continuous recursive application of iter won't unboundedly grow stack
;;;; the maximum stack depth only grows by 3 or 2(if the true branch is followed with no
;;;; false branches being evaluated), regardless of n
;;;; therefore, it is tail-recursive, maximum stack depth is O(1) for this procedure
