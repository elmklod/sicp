;;; why would they produce non-tail recursive execution procedure for sequences
;;; that blows stack? It makes little sense since the tail recursive one is actually
;;; not much harder to produce
;;; The alternative is straightforward, i.e. use fold-right to produce tail-rec version
;;; reverse is easily implementable in almost any language
(define (analyze-sequence exps)
  (define (sequentially proc1 proc2)
    (lambda (env) (proc1 env) (proc2 env)))
  (define (loop next-proc rest-procs)
    (if (null? rest-procs)
        next-proc
        (loop (sequentially (car rest-procs) ;;; swap them
		                        next-proc)       ;;; and get the
              (cdr rest-procs))))            ;;; version that
  (let ((procs (map analyze exps)))          ;;; does not
    (if (null? procs)                        ;;; blow stack
        (error "Empty sequence: ANALYZE"))
    (let ((rev-procs (reverse procs)))
     (loop (car rev-procs) (cdr rev-procs)))))
