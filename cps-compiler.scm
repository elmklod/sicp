When reading 5.5 I felt especially strange.
First preserving has additional parameter - registers list.
It relies on this to preserve necessary regs.
But! It is necessary to believe that analysis, which registers are needed and which are not, is correct
but it's made by a human, the author of the program.
What if it's proved by a compiler what is modified, needed?
It is actually very hard since it won't work for procedure applications where procedure is not yet defined.
Resort to the authors way of assuming everything is modified.
Also not clear how to preserve from just judging sequences.

But the way to do it is by writing CPS-style compiler
WIP

In fact preserving will still require the third parameter, to preserve side-effcts.
This third parameter is in fact the list of registers that should not be preserved.