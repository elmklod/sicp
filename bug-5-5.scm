(define (compile-lambda-body exp proc-entry)
  (let ((formals (lambda-parameters exp)))
    (append-instruction-sequences
     (make-instruction-sequence 
      '(env proc argl)
      '(env)
      `(,proc-entry
        (assign env 
                (op compiled-procedure-env)
                (reg proc))
        (assign env
                (op extend-environment)
                (const ,formals)
                (reg argl)
                (reg env))))
     (compile-sequence (lambda-body exp)
                       'val
                       'return))))

Notice how newly constructed instruction sequence needs env
register, although it modifies it before ANY access to it.
Now this is a bug, this sequence does not need env,
saving it is redundant.
But does this bug affect anything?
No, it does not, since the lambda body is evaluated only
during the procedure application, and, well,
the procedure application code is indeed subtle
There is literally NO checking, no preserving of registers done
by a compiler, and there is NO appending of sequnces, the
appending of procedure body is done without propagating its need for ANY register, it is propagated implicitly.

Env though unneeded is not propagated.

Argl though needed is not propagated but is implicitly expected
to always contain the correct data since the code to accumulate
arg values always precedes the procedure application and there
are no extra modifying instructions ever.

Proc even though is also needed, is not propagated at all but
since the code before entering the procedure body also needs proc, it is implicitly propagated as well.

Since the linkage in the lambda body is always return, it means
that continue is in fact implicitly required.

What's more, the need for it is not propagated as well and unlike other registers,
there are no guarantees that it will be preserved.
The compiler deals with it in two ways,
first it modifies
continue, mentioning that it will be modified,
and that removes the need for continue since modification
preceds the entering of the lambda body and does not need continue
The second way is explicit, it just states that it will need
continue register.
Therefore, the need is propagated, but it's done in different,
non-automated manner for each register.
Especially the argl which is absolutely implicit
and continue, where the need is propagated explicitly depeding
on other factors, which are not abstracted.

What could solve the continue and proc problem is something like preserving procedure which didn't append the second seq
instructions, only propagated the need.

But anyways 3 required registers are preserved.
argl, proc and continue

Notice that env register need is not propagated at all.
Therefore, it can be safely removed from the need list.

(define (compile-lambda-body exp proc-entry)
  (let ((formals (lambda-parameters exp)))
    (append-instruction-sequences
     (make-instruction-sequence 
      '(proc argl)  ;;;;; here
      '(env)
      `(,proc-entry
        (assign env 
                (op compiled-procedure-env)
                (reg proc))
        (assign env
                (op extend-environment)
                (const ,formals)
                (reg argl)
                (reg env))))
     (compile-sequence (lambda-body exp)
                       'val
                       'return))))


Now to check.
